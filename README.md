# Readme

This repository contains the mzML spectra used for the publication:

## Evaluation of air oxidized PAPC: A multi laboratory study by LC-MS/MS

### Zhixu Ni, Bebiana C. Sousa, Simone Colombo, Catarina B. Afonso, Tania Melo, Andrew R. Pitt, Corinne M. Spickett, Pedro Domingues, M. Ros rio Domingues, Maria Fedorova, Angela Criscuolo

### Free Radical Biology and Medicine, 2019

DOI : 10.1016/j.freeradbiomed.2019.06.013

View original publication on [sciencedirect.com](http://www.sciencedirect.com/science/article/pii/S0891584919303478)

### All data is released under a [Creative Commons license - Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)](https://creativecommons.org/licenses/by-nc-nd/4.0/)


# Download data

## Please visit [Downloads page](https://bitbucket.org/SysMedOs/oxpapc_spectra/downloads/)

+ Downloads page of this repository [https://bitbucket.org/SysMedOs/oxpapc_spectra/downloads/](https://bitbucket.org/SysMedOs/oxpapc_spectra/downloads/)
+ Raw files were converted into mzML files.
+ mzML files were zipped into .7z file using `7zip` software [https://www.7-zip.org](https://www.7-zip.org)
+ Please use the latest version of `7zip` to unzip the .7z file to get the .mzML file